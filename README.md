#  Bubble Analyser

Bubble Analyser is a Matlab Application for processing and quantifying bubble images, normally taken for assessing froth flotation equipment.

## Installation

You can simply download a copy of the latest version from the GitLab repository as a .zip (or equivalent) file. Unzip the file and keep the unpacked folder wherever you prefer—We recommend to put it in the default Matlab folder. Once you have decided where to keep it, make sure you add this location to your Matlab path. For help on doing this, please refer to this [link](https://www.mathworks.com/help/matlab/ref/addpath.html).

## Usage

If you have added the ‘Bubble Analyser’ folder to your Matlab path, you can run the APP by:
```>> BubbleAnalyserApp()```

Bubble Analyser was developed for Matlab 2020, or newer. However, it might run with older versions of Matlab. Bubble Analyser uses the following Toolboxes: Image Processing and Signal Processing.

## Contributing
We invite researchers to join as developers to the repository so they can contribute making Bubble Analyser better. Please get in touch with the Owner of the repository to be added as a member. New members will be added as Developers, meaning that they will not be able to push to `master`.

Once joined, please open an issue first to discuss what you would like to change. You can also create your own feature branch and then create a Merge Request so your new feature is added to the `master` branch.

## Cite this work
To reference the work please do it as:

+ Cite software/repository

@misc{BubbleAnalyser2021,
  author = {Mesa, Diego and Quintanilla, Paulina and Reyes, Francisco},
  title = {{Bubble Analyser software}},
  year = {2021},
  publisher = {GitLab},
	url = {https://gitlab.com/frreyes1/bubble-sizer}    
}

+ Cite journal paper

@article{Mesa2022,
author = {Mesa, Diego and Quintanilla, Paulina and Reyes, Francisco},
doi = {https://doi.org/10.1016/j.mineng.2022.107497},
issn = {0892-6875},
journal = {Minerals Engineering},
pages = {107497},
title = {{Bubble Analyser — An open-source software for bubble size measurement using image analysis}},
volume = {180},
year = {2022}
}

## License
Bubble Analyser is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 only of the License.

Bubble Analyser is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Bubble Analyser.  If not, see <https://www.gnu.org/licenses/>.
